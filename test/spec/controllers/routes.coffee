'use strict'

describe 'Controller: RoutesCtrl', ->

  # load the controller's module
  beforeEach module 'gisCodeChallengeApp'

  RoutesCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    RoutesCtrl = $controller 'RoutesCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
