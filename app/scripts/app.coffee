'use strict'

angular
  .module('gisCodeChallengeApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
  ])
  .config ($routeProvider) ->
    $routeProvider
      .when '/',
        templateUrl: 'views/main.html'
        controller: 'MainCtrl'
      .when '/routes',
        templateUrl: 'views/routes.html'
        controller: 'RoutesCtrl'
      .otherwise
        redirectTo: '/'

